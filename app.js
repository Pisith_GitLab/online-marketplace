const express = require('express');
const bodyParser = require('body-parser')
require('dotenv').config();
const { dbConnection } = require('./dataSources/db');
const passport = require('passport');
const authRoute = require('./routes/authRoute');
const productRoute = require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute');
const cartRoute = require('./routes/cartRoute');
const checkoutRoute = require('./routes/checkoutRoute');
const syncDatabase = require('./models/sync');

const app = express();
const PORT = process.env.PORT || 3000;

// Database Connection
dbConnection();
syncDatabase();

// Passport middleware
// Initialize Passport and Passport JWT strategy
app.use(passport.initialize());
require('./config/passport')(passport);

// bodyParser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.status(200).json({message: 'Welcome to Online-Marketplace mf!!!'});
})

app.use('/api/v1/auth', authRoute);

app.use('/api/v1/product', productRoute);

app.use('/api/v1/order', orderRoute);

app.use('/api/v1/cart', cartRoute);

app.use('/api/v1/checkout', checkoutRoute);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
})