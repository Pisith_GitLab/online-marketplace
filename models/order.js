const { sq } = require('../dataSources/db');
const { DataTypes } = require('sequelize');

const Order = sq.define('Order', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  userId: {
    type: DataTypes.UUID,
    allowNull: false,
    validate: {
      isUUID: 4, // Validate that userId is in UUIDv4 format
    },
  },
  productId: {
    type: DataTypes.UUID,
    allowNull: false,
    validate: {
      isUUID: 4, // Validate that userId is in UUIDv4 format
    },
  },
  qty: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
})

module.exports = Order;