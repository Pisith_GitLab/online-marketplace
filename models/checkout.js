const { sq } = require('../dataSources/db');
const { DataTypes } = require('sequelize');

const Checkout = sq.define('Checkout', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
    primaryKey: true
  },
  totalPrice: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  userId: {
    type: DataTypes.UUID,
    allowNull: false
  }
})

module.exports = Checkout;