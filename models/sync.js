const { sq } = require('../dataSources/db');
const User = require('../models/user');
const Product = require('../models/product');
const Cart = require('./cart');
const Checkout = require('../models/checkout');

const syncDatabase = async () => {
  try {
    await sq.authenticate();
    console.log('Connection has been established successfully.');

    // Relationship
    User.hasMany(Cart);
    User.hasMany(Checkout);

    Product.hasMany(Cart);

    Cart.belongsTo(User, { foreignKey: 'userId' });
    Cart.belongsTo(Product, { foreignKey: 'productId' });

    Checkout.belongsTo(User, { foreignKey: 'userId' });

    // Sync Tables
    sq.sync({
      force: false
    }).then(() => {
      console.log('Tables synced successfully.');
    }).catch((err) => {
      console.error('Error syncing tables: ', err);
    })

  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

module.exports = syncDatabase;