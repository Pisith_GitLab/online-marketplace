const express = require('express');
const productController = require('../controllers/productController');
const passport = require('passport');

const router = express.Router();

// Passport JWT authentication middleware
router.use(passport.authenticate('jwt', { session: false }));

router.post('/', productController.create);
router.get('/', productController.findAll);
router.get('/search', productController.search);
router.get('/:id', productController.findById);
router.put('/:id', productController.updateById);
router.delete('/:id', productController.deleteById);

module.exports = router;