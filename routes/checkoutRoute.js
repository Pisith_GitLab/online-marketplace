const express = require('express');
const checkoutController = require('../controllers/checkoutController');
const passport = require('passport');

const router = express.Router();

// use passport jwt middleware
router.use(passport.authenticate('jwt', { session: false }));

router.post('/', checkoutController.checkout);


module.exports = router;

