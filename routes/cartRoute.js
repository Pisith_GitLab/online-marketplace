const express = require('express');
const cartController = require('../controllers/cartController');
const passport = require('passport');

const router = express.Router();

// use passport jwt
router.use(passport.authenticate('jwt', { session: false }));

router.post('/', cartController.create);

module.exports = router;