const express = require('express');
const orderController = require('../controllers/orderController');
const passport = require('passport');

const router = express.Router();

// Passport JWT authentication middleware
router.use(passport.authenticate('jwt', { session: false }));

router.post('/', orderController.create);
router.get('/', orderController.findAll);
router.get('/:id', orderController.findById)
router.delete('/:id', orderController.deletedById);

module.exports = router;