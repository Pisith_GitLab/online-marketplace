const Product = require('../models/product');
const { Op } = require('sequelize');

const create = async (req, res) => {
  try {
    const {name, price, description, url } = req.body;
    const product = await Product.findOne({where: { name }});
    if (product) {
      return res.status(409).json({message: 'Product already exist'});
    }
    const newProduct = await Product.create({name, price, description, url});
    return res.status(201).json({message: 'Product created successfully', product: newProduct});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const findAll = async (req, res) => {
  try {
    const products = await Product.findAll();
    return res.status(200).json({products});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const findById = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({message: 'Product not found'});
    }
    return res.status(200).json({product});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error}); 
  }
}

const updateById = async (req, res) => {
  try {
    const id = req.params.id;
    const {name, price, description, url} = req.body;

    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({message: 'Product not found'});
    }

    await product.update({name, price, description, url});
    return res.status(200).json({message: 'Product updated successfully'});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error}); 
  }
}

const deleteById = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({message: 'Product not found'});
    }
    await product.destroy();
    return res.status(200).json({message: 'Product deleted successfully'});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

// Search products
const search = async (req, res) => {
  try {
    const query = req.query.name;
    console.log(`query : ${query}`);
    const products = await Product.findAll({
      where: {
        name: {
          [Op.iLike]: `%${query}%`
        }
      }
    })
    res.status(200).json({products});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error})
  }
}

module.exports = {
  create,
  findAll,
  findById,
  updateById,
  deleteById,
  search
}