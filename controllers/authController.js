const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bycrypt = require('bcrypt');

const register = async (req, res) => {
  try {
    const { username, email, password } = req.body;
    const user = await User.findOne({ where: { email }});
    if (user) {
      return res.status(400).json({ message: 'User already exist' });
    }
    const salt = await bycrypt.genSalt(10);
    const hashedPassword = await bycrypt.hash(password, salt);
    const newUser = await User.create({username, email, password: hashedPassword});
    return res.status(201).json({message: 'User registered successfully', user: newUser});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ where: { email }});
    if (!user) {
      return res.status(404).json({message: 'User does not exist'});
    }

    const isPasswordValid = await bycrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(400).json({message: 'Invalid password'});
    }

    const token = await jwt.sign({sub: user.id}, process.env.JWT_SECRET_KEY, {expiresIn: '1h'});
    return res.status(200).json({message: 'Login successfully', token});
  } catch (error) {
    
  }
}

module.exports = {
  register,
  login
}