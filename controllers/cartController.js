const Cart = require('../models/cart');

const create = async (req, res) => {
  try {
    const { userId, productId, qty } = req.body;

    let cartItem = await Cart.findOne({
      where: {
        userId,
        productId
      }
    })
    if (cartItem) {
      cartItem.qty += qty;
      await cartItem.save();
    } else {
      cartItem = await Cart.create({userId, productId, qty});
    }
    res.status(200).json({
      success: true,
      message: 'Product added to cart',
      cartItem
    })
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

module.exports = {
  create
}