const Order = require('../models/order');
const User = require('../models/user');
const Product = require('../models/product');

const create = async (req, res) => {
  try {
    const { userId, productId, qty } = req.body;

    const user = await User.findByPk(userId);
    const product = await Product.findByPk(productId);
    if (!user || !product) return res.status(404).json({message: 'User or Product not found'});

    const order = await Order.create({userId, productId, qty});
    return res.status(201).json({message: 'Order created successfully', order})
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const findAll = async (req, res) => {
  try {
    const orders = await Order.findAll(
      {include: [{model: User}, {model: Product}]}
    );
    return res.status(200).json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const findById = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findByPk(id, {
      include: [{ model: User }, { model: Product }],
    });
    if (!order) return res.status(404).json({message: 'Order not found'});
    return res.status(200).json({order});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

const deletedById = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findByPk(id);
    if (!order) return res.status(404).json({message: 'Order not found'});
    await order.destroy();
    return res.status(200).json({message: 'Order deleted successfully'});
  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

module.exports = {
  create,
  findAll,
  findById,
  deletedById
}