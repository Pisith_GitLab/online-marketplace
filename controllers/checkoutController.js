const Checkout = require('../models/checkout');
const Cart = require('../models/cart');
const Product = require('../models/product');

const checkout = async (req, res) => {
  try {
    const userId = req.user.id;
    const cartItems = await Cart.findAll({
      where: {
        userId,
      },
      include: [{ model: Product }],
    })
    let totalPrice = 0;
    cartItems.forEach(item => {
      totalPrice += item.qty * item.Product.price;
    })

    const newCheckout = await Checkout.create({
      totalPrice,
      userId
    })

    await Cart.destroy({where: {userId}});

    return res.status(201).json({
      success: true,
      message: 'Checkout successfully',
      checkout: newCheckout
    });

  } catch (error) {
    console.error(error);
    res.status(500).json({message: 'Internal Server Error', error});
  }
}

module.exports = {
  checkout
}